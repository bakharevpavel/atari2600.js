var rom = null;
var romSize = null;

var scanlineCounter = 0;

var cpu = new CPU();
var tia = new TIA(document.querySelector('canvas'));
var pia = new PIA();
var display = new Display(document.querySelector('canvas'));

var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
window.requestAnimationFrame = requestAnimationFrame;


function processFile()  {

	console.log('Reading process started!');

	var file = document.getElementById('file').files[0];

	var reader = new FileReader();
	reader.readAsArrayBuffer(file);

	reader.onloadend = function(evt) {
		var data = this.result;
	
		// Lets mirror our data to match 64K rom

		rom = new Uint8Array(65536);
		romSize = data.byteLength
		for(var i = 0; i < 65536; i += romSize) {
			rom.set((new Uint8Array(data)), i);
			console.log(123);
		}
		read();
	}
}

function swap16(val) {
    return ((val & 0xFF) << 8) | ((val >> 8) & 0xFF);
}

function read() {
	var clock = 0;
	//requestAnimationFrame(display.nextFrame);

	// function nextFrame() {
	// 	setTimeout(function() {
	// 		requestAnimationFrame(display.nextFrame);
	// 	}, 1000 / 60);
	// 	nextFrame();
	// }
	

	function nextFrame() {
		setTimeout(function() {
			//requestAnimationFrame(display.nextFrame);
			display.nextFrame().then(function() {
				//console.log(2);
				nextFrame();
			}).catch(function(data) {
				console.log(data.pc);
				console.log(data.opcode);
				throw data.error;
			});
			
		}, 1000 /* / 60 */);
		
	}

	nextFrame();

}

function toBinary(val) {
	val = val.toString(2).replace('-', ''); // Borked
	if(val.length < 8) {
		for(var i = 0, length = val.length; i < 8 - length; i++) {
			val = '0' + val;
		}
	}
	return val;
}

function toBinaryArray(val) {
	return toBinary(val).split('');
}

function toBinaryReverse(val) {
	val = val.toString(2).replace('-', ''); // Borked
	if(val.length < 8) {
		for(var i = 0, length = val.length; i < 8 - length; i++) {
			val = '0' + val;
		}
	}
	return val.split('').reverse().join('');
}

function toBinaryReverseArray(val) {
	return toBinaryReverse(val).split('');
}

function convertHex(hex){
	var c = [];
    hex = hex.replace('#','');
    c[0] = parseInt(hex.substring(0,2), 16);
    c[1] = parseInt(hex.substring(2,4), 16);
   	c[2] = parseInt(hex.substring(4,6), 16);
    return c;
}