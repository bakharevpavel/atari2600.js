function CPU() {

	// Private

	var locked = false;

	var cycle = 0;

	// Public

	var self = this;

	self.lock = function() {
		locked = true;
	};

	self.unlock = function() {
		locked = false;
		cycle = 0; // ????
	};

	self.pulse = function() {
		if(locked == false && cycle <= 0) {
			console.log(rom[register.PC].toString(16).toUpperCase());
			opcode[rom[register.PC].toString(16).toUpperCase()]();
			register.PC++;
		}
		cycle--;
	};

	self.setCycle = function(val) {
		cycle = val;
	};
}