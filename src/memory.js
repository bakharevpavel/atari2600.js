// CTRLPF

var memory = {
	0x00: { // VSYNC
		write: function(val) {
			this.value = val;
			if(toBinary(this.value).split('').reverse()[1] == '1') {
				//display.skipScanline += 1;
				tia.vsync = true;
			} else {
				tia.vsync = false;
			}
			console.log('vsync changed ' + tia.vsync);
		},
		value: false
	},
	0x01: { // VBLANK
		write: function(val) {
			this.value = val;
			//tia.wait(37);
			//tia.wait(64);
		},
		value: 0
	},
	0x02: { // WSYNC
		write: function(val) {
			this.value = val;
			cpu.lock();
		},
		value: 0
	},
	0x03: { // RSYNC
		write: function(val) {
			this.value = val;
		},
		value: 0
	},
	0x08: { // COLUPF
		write: function(val) {
			this.value = val;
			//var bit = toBinary(this.value).split('').reverse();
			tia.playfield.setColor(toBinaryReverseArray(this.value));
		},
		value: 0
	},
	0x09: { //COLUBK
		write: function(val) {
			this.value = val;
			tia.background.setColor(toBinaryReverseArray(this.value));
		},
		value: 0
	},
	0x0A: { // CTRLPF
		write: function(val) {
			//console.log(val);
			this.value = val;
			var bit = toBinaryReverseArray(this.value);
			//console.log(bit);
			if(bit[0] == '1') {
				tia.playfield.reflect = 1;
			} else {
				tia.playfield.reflect = 0;
			}
		},
		value: 0
	},
	0x0D: { // PF0
		write: function(val) {
			this.value = val;
			tia.playfield.pf0 = toBinary(this.value);
		},
		value: 0
	},
	0x0E: { // PF1
		write: function(val) {
			this.value = val;
			tia.playfield.pf1 = toBinary(this.value);
		},
		value: 0
	},
	0x0F: { // PF2
		write: function(val) {
			this.value = val;
			tia.playfield.pf2 = toBinary(this.value);
		},
		value: 0
	},
	0x0284: { // INTIM
		write: function(val) {
			this.value = val;
		},
		value: (Math.random() * 256)//0 // Random value?
	},
	0x0294: { // TIM1T
		write: function(val) {
			//this.value = val;
			pia.setTimer(1);
		},
		value: 0
	},
	0x0295: { // TIM8T
		write: function(val) {
			//this.value = val;
			pia.setTimer(8);
		},
		value: 0
	},
	0x0296: { // TIM64T
		write: function(val) {
			//this.value = val;
			pia.setTimer(64);
		},
		value: 0
	},
	0x0297: { // T1024T
		write: function(val) {
			//this.value = val;
			pia.setTimer(1024);
		},
		value: 0
	} 
};