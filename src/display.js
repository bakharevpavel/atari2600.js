function Display(canvas) {

	// Private

	// canvas.width = 160;

	// canvas.height = 192;

	var ctx = canvas.getContext('2d');

	var w = canvas.width;

	var h = canvas.height;

	console.log(w);

	var data = tia.ctx.getImageData(0, 0, w, h);

	var scanline = 0;

	var clock = 0;

	ctx.fillStyle = 'black';

	ctx.fillRect(0, 0, w, h);

	// Public

	var self = this;

	self.nextFrame = function() {


	var promise = new Promise(function(resolve, reject) {

	try {
		var cpuCounter = 0;
		for(scanline = 0; scanline < 70; scanline++) { // 40 // 4
			for(clock = 0; clock < 228; clock += 3) {
				cpu.pulse();
				pia.clockPassed(3);
			}
			cpu.unlock();
		}
	
		for(scanline = 0; scanline < 192; scanline++) { // 162
				for(clock = 0; clock < 68; clock += 3) {
					cpu.pulse();
					pia.clockPassed(3);
				}
				
				for(clock = 0; clock < 160; clock++) {

					var pf0 = toBinaryReverseArray(tia.playfield.pf0);
					var pf1 = toBinaryReverseArray(tia.playfield.pf1);
					var pf2 = toBinaryReverseArray(tia.playfield.pf2);

					// if(tia.vsync == false) {
					// 	console.log('falsed');
					// 	continue;
					// }
					if(clock <= 16) {
						if(clock < 4) {
							if(pf0[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock <= 8) {
							if(pf0[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock <= 12) {
							if(pf0[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock <= 16) {
							if(pf0[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 16 && clock <= 48) {
						if(clock > 16 && clock < 20) {
							if(pf1[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 20 && clock < 24) {
							if(pf1[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 24 && clock < 28) {
							if(pf1[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 28 && clock < 32) {
							if(pf1[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 32 && clock < 36) {
							if(pf1[3] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 36 && clock < 40) {
							if(pf1[2] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 40 && clock < 44) {
							if(pf1[1] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock >= 44 && clock < 48) {
							if(pf1[0] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 48 && clock < 80) {
						if(clock > 48 && clock < 52) {
							if(pf2[0] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 52 && clock < 56) {
							if(pf2[1] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 56 && clock < 60) {
							if(pf2[2] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 60 && clock < 64) {
							if(pf2[3] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 64 && clock < 68) {
							if(pf2[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 68 && clock < 72) {
							if(pf2[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 72 && clock < 76) {
							if(pf2[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 76 && clock < 80) {
							if(pf2[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 80 && clock < 96 && tia.playfield.reflect == 0) { // REF = 0
						if(clock > 80 && clock < 84) {
							if(pf0[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 84 && clock < 88) {
							if(pf0[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 88 && clock < 92) {
							if(pf0[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 92 && clock < 96) {
							if(pf0[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 96 && clock < 128 && tia.playfield.reflect == 0) {
						if(clock > 96 && clock < 100) {
							if(pf1[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 100 && clock < 104) {
							if(pf1[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 104 && clock < 108) {
							if(pf1[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 108 && clock < 112) {
							if(pf1[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 112 && clock < 116) {
							if(pf1[3] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 116 && clock < 120) {
							if(pf1[2] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 120 && clock < 124) {
							if(pf1[1] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 124 && clock < 128) {
							if(pf1[0] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 128 && clock < 160 && tia.playfield.reflect == 0) {
						if(clock > 128 && clock < 132) {
							if(pf2[0] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 132 && clock < 136) {
							if(pf2[1] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 136 && clock < 140) {
							if(pf2[2] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 140 && clock < 144) {
							if(pf2[3] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 144 && clock < 148) {
							if(pf2[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 148 && clock < 152) {
							if(pf2[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 152 && clock < 156) {
							if(pf2[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 156 && clock < 160) {
							if(pf2[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 80 && clock < 112 && tia.playfield.reflect == 1) { // REF = 1
						if(clock > 80 && clock < 84) {
							if(pf2[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 84 && clock < 88) {
							if(pf2[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 88 && clock < 92) {
							if(pf2[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 92 && clock < 96) {
							if(pf2[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 96 && clock < 100) {
							if(pf2[3] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 100 && clock < 104) {
							if(pf2[2] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 104 && clock < 108) {
							if(pf2[1] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 108 && clock < 112) {
							if(pf2[0] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 112 && clock < 144 && tia.playfield.reflect == 1) {
						if(clock > 112 && clock < 116) {
							if(pf1[0] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 116 && clock < 120) {
							if(pf1[1] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 120 && clock < 124) {
							if(pf1[2] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 124 && clock < 128) {
							if(pf1[3] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 128 && clock < 132) {
							if(pf1[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 132 && clock < 136) {
							if(pf1[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 136 && clock < 140) {
							if(pf1[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 140 && clock < 144) {
							if(pf1[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					} else if(clock > 144 && clock < 160 && tia.playfield.reflect == 1) {
						if(clock > 144 && clock < 148) {
							if(pf0[7] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 148 && clock < 152) {
							if(pf0[6] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 152 && clock < 156) {
							if(pf0[5] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						} else if(clock > 156 && clock < 160) {
							if(pf0[4] == '1') {
								var c = convertHex(tia.playfield.color);
							} else {
								var c = convertHex(tia.background.color);
							}
						}
					}

					var pixelindex = Math.floor((scanline * w + ( 2 * clock)) * 4);
					data.data[pixelindex] = c[0];
					data.data[pixelindex + 1] = c[1];
					data.data[pixelindex + 2] = c[2];
					
					var pixelindex = Math.floor((scanline * w + ( 2 * clock - 1)) * 4);
					data.data[pixelindex] = c[0];
					data.data[pixelindex + 1] = c[1];
					data.data[pixelindex + 2] = c[2];


					cpuCounter++;
					if(cpuCounter > 2) { //3
						cpu.pulse();
						cpuCounter = 0;
					}
					pia.clockPassed();
				}

			cpu.unlock();
		}

		ctx.putImageData(data, 0, 0);

		// for(scanline = 0; scanline < 30; scanline++) {
		// 	for(clock = 0; clock < 228; clock += 3) {
		// 		cpu.pulse();
		// 		pia.clockPassed(3);
		// 	}
		// 	cpu.unlock();
		// }

		//console.log(1);
		resolve(true)

		} catch(e) {
			reject({
				pc: register.PC,
				opcode: rom[register.PC].toString(16).toUpperCase(),
				error: e
			});
		}		
		});
		return promise;
	};
};