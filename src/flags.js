var flags = {
	'-': 1, // Not used?
	B: 0, // Break flag
	C: 0, // Carry
	D: 1, // Decimal mode
	I: 0 , // Interrupt disable bit
	N: 0, // Negative/Sign
	V: 0, // Overflow
	Z: 0, // Zero
};