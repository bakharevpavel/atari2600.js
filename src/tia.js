function TIA(canvas) {

	// Private

	var ctx = canvas.getContext('2d');

	var w = canvas.width;

	var h = canvas.height;

	var scanlineHeight = h / 262;

	var clockWidth = w / 228;

	var skipScanline = 0;

	ctx.fillStyle = 'black';

	ctx.fillRect(0, 0, w, h);


	function luminateColor(hex, lum) {
		//console.log(lum);
		if(hex == '#000000' && lum == 1) { // LET IT BLEED
			//console.log('whitened');
			return '#FFFFFF';
		}

		hex = String(hex).replace(/[^0-9a-f]/gi, '');
		if (hex.length < 6) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
		}

		lum = lum || 0;

		var rgb = "#", c, i;
		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i * 2,2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += ("00" + c).substr(c.length);
		}
		// if(lum == 0) {
		// 	console.log(hex);
		// 	console.log(rgb);
		// 	console.log('-------------');
		// 	clearInterval(sadad);
		// }

		return rgb;
	};

	function getColor(val) {
		var color = null;
		var luminance = null;
		switch(val[3] + val[2] + val[1]) {
			case '000':
				luminance = 0;
				break;
			case '001':
				luminance = 0.142;
				break;
			case '010':
				luminance = 0.284;
				break;
			case '011':
				luminance = 0.426;
				break;
			case '100':
				luminance = 0.568;
				break;
			case '101':
				luminance = 0.710;
				break;
			case '110':
				luminance = 0.852;
				break;
			case '111':
				luminance = 1;
				break;
		}
		switch(val[7] + val[6] + val[5]  + val[4]) {
			case '0000':
				color = '#000000';
				break;
			case '0001':
				color = '#444400';
				break;
			case '0010':
				color = '#702800';
				break;
			case '0011':
				color = '#841800';
				break;
			case '0100':
				color = '#880000';
				break;
			case '0101':
				color = '#78005c';
				break;
			case '0110':
				color = '#480078';
				break;
			case '0111':
				color = '#140084';
				break;
			case '1000':
				color = '#000088';
				break;
			case '1001':
				color = '#00187c';
				break;
			case '1010':
				color = '#002c5c';
				break;
			case '1011':
				color = '#003c3c';
				break;
			case '1100':
				color = '#003c00';
				break;
			case '1101':
				color = '#143800';
				break;
			case '1110':
				color = '#2c3000';
				break;
			case '1111':
				color = '#442800';
				break;
			default:
				console.log('not found');
				console.log(val);
				color = '#000000';
				break;
		}

		return luminateColor(color, luminance);
	}

	// Public

	var self = this;

	self.ctx = ctx;

	self.w = w;

	self.h = h;

	self.skipScanline = 0;

	self.scanlineHeight = h / 262;

	self.clockWidth = w / 228;

	self.skipScanline = 0;

	self.background = {
		color: '#000000'
	};

	self.background.setColor = function(val) {
		self.background.color = getColor(val);
	};

	self.background.getColor = function() {
		return self.background.color;
	};

	self.ball = {
		size: 1
	};

	self.clock = 0;

	self.playfield = {
		colorType: 0,
		color: '#000000',
		reflect: 0,
		proprity: 0,
		pf0: 0,
		pf1: 0,
		pf2: 0
	};

	self.playfield.setColor = function(val) {
		self.playfield.color = getColor(val);
	};

	self.playfield.getColor = function() {
		return self.playfield.color;
	};

	self.vsync = false;
};