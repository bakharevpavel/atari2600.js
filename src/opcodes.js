// Check if conditional branches should reset their respective flags to the specific value
// Move flag check to separate function

function createMemoryIfNotExist(addr) {
	if(typeof memory[addr] == 'undefined') {
		memory[addr] = {};
		memory[addr].write = function(val) {
			this.value = val;
		};
		memory[addr].value = 0;
	}
}

function isNextPage(pc1, pc2) { // Borked?
	return ('000' + pc1.toString(16)).slice(-4).split('')[0] != ('000' + pc2.toString(16)).slice(-4).split('')[0];
}

var opcode = {
	'01': function() {
		var addr = rom[++register.PC] + register.X;
		createMemoryIfNotExist(addr);
		register.A = register.A || memory[memory[addr]].value;
		cpu.setCycle(6);
	},
	'9': function() { // ORA #nn
		register.A = rom[++register.PC] || register.A;
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}		
	},
	'10': function() { // BPL nnn
		if(flags.N == 1) {
			register.PC++;
			cpu.setCycle(2);
		} else {
			var num = rom[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}
	},
	'4C': function() { // JMP nnnn
		var addr = swap16(((rom[++register.PC] & 0xFF) << 8) | (rom[++register.PC] & 0xFF));
		console.log(addr);

		addr = addr.toString(16).split('');
		console.log(addr);
		//throw new Error();
		var num = parseInt(addr.join(''), 16) - 1;
		register.PC = num;
		cpu.setCycle(3);
	},
	'29': function() { // AND #nn
		register.A = rom[++register.PC] && register.A;
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'78': function() { // SEI
		flags.I = 1;
		cpu.setCycle(2);
	},
	// '81': function() { // STA (nn, X)
	// 	var addr = rom[++register.PC] + register.X;
	// 	createMemoryIfNotExist(addr);
	// 	memory[memory[addr]].write(register.A);
	// 	cpu.setCycle(6);
	// 	console.log('asdadsa')
	// },
	'84': function() { // STY
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].write(register.Y);
		cpu.setCycle(3);
	},
	'85': function() { // STA nn
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].write(register.A);
		cpu.setCycle(3);
	},
	'86': function() { // STX nn
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].write(register.X);
		cpu.setCycle(3);
	},
	'88': function() { // DEY
		register.Y--;
		cpu.setCycle(2);
		if(register.Y == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.Y < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'8A': function() { // TXA
		register.A = register.X;
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'8D': function() { // STA nnnn
		var addr = swap16((((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)));
		createMemoryIfNotExist(addr);
		memory[addr].write(register.A);
		cpu.setCycle(4);
	},
	'90': function() {
		if(flags.C == 1) {
			register.PC++;
			cpu.setCycle(2);
		} else {
			var num = rom[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}
	},
	'95': function() { // STA nn, X
		var addr = rom[++register.PC] + register.X;
		createMemoryIfNotExist(addr);
		memory[addr].write(register.A);
		cpu.setCycle(4);
	},
	'9A': function() { // TXS
		register.S = register.X;
		cpu.setCycle(2);
	},
	'A0': function() { // LDY
		register.Y = rom[++register.PC];
		cpu.setCycle(2);
		if(register.Y == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.Y < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A2': function() { // LDX
		register.X = rom[++register.PC];
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A5': function() { // LDA nn
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr); // Error?
		register.A = memory[addr].value;
		console.log('-----------');
		console.log(register.PC);
		cpu.setCycle(3);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A6': function() { // LDX nn
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr);
		register.X = memory[addr].value;
		cpu.setCycle(3);
		// console.log(memory[addr].value);
		// console.log(memory[addr].value.toString(2));
		// clearInterval(213123);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A9': function() { // LDA
		register.A = rom[++register.PC];
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'AA': function() { // TAX
		register.X = register.A;
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'AD': function() { // LDA nnnn
		var addr = swap16((((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)));
		//createMemoryIfNotExist(addr);
		register.A = memory[addr].value;
		cpu.setCycle(4);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	// 'B1': function() { // LDA (nn),Y
	// 	var addr = memory[rom[++register.PC]] + register.Y;
	// 	createMemoryIfNotExist(addr); // Error?
	// 	register.A = memory[addr].value;
	// 	console.log('-----------');
	// 	console.log(register.PC);
	// 	cpu.setCycle(5 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
	// 	if(register.A == 0) {
	// 		flags.Z = 1;
	// 	} else {
	// 		flags.Z = 0;
	// 	}
	// 	if(register.A < 0) {
	// 		flags.N = 1;
	// 	} else {
	// 		flags.N = 0;
	// 	}
	// },
	'BD': function() { // LDA nnnn, x // Possible playground fall error?
		var d = ((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff);
		//console.log(d + register.X);
		//clearInterval(dasda);
		var addr = swap16(d) + register.X;
		createMemoryIfNotExist(addr);
		register.A = rom[addr]; // rom[addr]
		console.log('ssssssssss' + register.A);
		cpu.setCycle(4); // Add cycle
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}	
	},
	'C6': function() { // DEC nn
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].value--;
		cpu.setCycle(5);
		if(memory[addr].value == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(memory[addr].value < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}	
	},
	'C9': function() {
		var comparable = rom[++register.PC];
		cpu.setCycle(2);
		if(register.A - comparable == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A - comparable < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
		if(register.A - comparable >= 0) {
			flags.C = 1;
		} else {
			flags.C = 0;
		}
	},
	'CA': function() { // DEX
		register.X--;
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'D0': function() { // BNE
		if(flags.Z == 1) {
			register.PC++;
			cpu.setCycle(2); // Add cycle
		} else {
			var num = rom[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}
	},
	'D8': function() { // CLD
		flags.D8 = 0;
		cpu.setCycle(2);
	},
	'E6': function() { // INC nn
		var addr = rom[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].value++;
		cpu.setCycle(5);
		if(memory[addr].value == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(memory[addr].value < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}	
	},
	'E8': function() { // INCX
		register.X++;
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'EA': function() { // NOP
		cpu.setCycle(2);
	},
	'F0': function() { // BEQ
		if(flags.Z == 0) {
			register.PC++;
			console.log('jump');
			cpu.setCycle(2); // Add cycle
		} else {
			var num = rom[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			console.log('jumped');
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}		
	} 
};

// var opcode = {
// 	'01': function() {
// 		register.A = register.A | rom[rom[rom[++register.PC] + register.X]];
// 	},
// 	'05': function() {
// 		register.A = register.A | rom[rom[++register.PC]];
// 	},
// 	'08': function() {
// 		stack.push(register.P);
// 		flags.S--;
// 	},
// 	'09': function() {
// 		register.A = register.A | rom[++register.PC];
// 	},
// 	'0A': function() {
// 		register.A = register.A << 1;
// 	},
// 	'0D': function() {
// 		register.A = register.A | rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))];
// 	},
// 	'11': function() {
// 		register.A = register.A | rom[rom[rom[++register.PC]] + register.Y];
// 	},
// 	'15': function() {
// 		register.A = register.A | rom[rom[++register.PC] + register.X];
// 	},
// 	'19': function() {
// 		register.A = register.A | rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y];
// 	},
// 	'1D': function() {
// 		register.A = register.A | rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X];
// 	},
// 	'21': function() {
// 		register.A = register.A & rom[rom[rom[++register.PC] + register.X]];
// 	},
// 	'25': function() {
// 		register.A = register.A & rom[rom[++register.PC]];
// 	},
// 	'28': function() {
// 		flags.S++;
// 		register.P = stack.pop();
// 	},
// 	'29': function() {
// 		register.A = register.A & rom[++register.PC];
// 	},
// 	'2D': function() {
// 		register.A = register.A & rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))];
// 	},
// 	'31': function() {
// 		register.A = register.A & rom[rom[rom[++register.PC]] + register.Y];
// 	},
// 	'35': function() {
// 		register.A = register.A & rom[rom[++register.PC] + register.X];
// 	},
// 	'39': function() {
// 		register.A = register.A & rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y];
// 	},
// 	'3D': function() {
// 		register.A = register.A & rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X];
// 	},
// 	'41': function() {
// 		register.A = register.A ^ rom[rom[rom[++register.PC] + register.X]];
// 	},
// 	'45': function() {
// 		register.A = register.A ^ rom[rom[++register.PC]];
// 	},
// 	'48': function() {
// 		stack.push(register.A);
// 		flags.S--;
// 	},
// 	'49': function() {
// 		register.A = register.A ^ rom[++register.PC];
// 	},
// 	'4D': function() {
// 		register.A = register.A ^ rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))];
// 	},
// 	'51': function() {
// 		register.A = register.A ^ rom[rom[rom[++register.PC]] + register.Y];
// 	},
// 	'55': function() {
// 		register.A = register.A ^ rom[rom[++register.PC] + register.X];
// 	},
// 	'59': function() {
// 		register.A = register.A ^ rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y];
// 	},
// 	'5D': function() {
// 		register.A = register.A ^ rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X];
// 	},
// 	'61': function() {
// 		register.A += (flags.C + rom[rom[rom[++register.PC] + register.X]]);
// 	},
// 	'65': function() {
// 		register.A += (flags.C + rom[rom[++register.PC]]);
// 	},
// 	'68': function() {
// 		flags.S++;
// 		register.A = stack.pop();
// 	},
// 	'69': function() {
// 		register.A += (flags.C + rom[++register.PC]);
// 	},
// 	'6D': function() {
// 		register.A += (flags.C + rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))]);
// 	},
// 	'71': function() {
// 		register.A += (flags.C + rom[rom[rom[++register.PC]] + register.Y]);
// 	},
// 	'75': function() {
// 		register.A += (flags.C + rom[rom[++register.PC] + register.X]);
// 	},
// 	'78': function() {
// 		flags.I = 1;
// 	},
// 	'79': function() {
// 		register.A += (flags.C + rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y]);
// 	},
// 	'7D': function() {
// 		register.A += (flags.C + rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X]);
// 	},
// 	'81': function() {
// 		rom[rom[rom[++register.PC] + register.X]] = register.A;
// 	},
// 	'84': function() {
// 		rom[rom[++register.PC]] = register.Y;
// 	},
// 	'85': function() {
// 		rom[rom[++register.PC]] = register.A;
// 	},
// 	'86': function() {
// 		rom[rom[++register.PC]] = register.X;
// 	},
// 	'88': function() {
// 		register.Y -= 1;
// 	},
// 	'8A': function() {
// 		register.A = register.X;
// 	},
// 	'8C': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))] = register.Y;
// 	},
// 	'8D': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))] = register.A;
// 	},
// 	'8E': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))] = register.X;
// 	},
// 	'91': function() {
// 		rom[rom[rom[++register.PC]] + register.Y] = register.A;
// 	},
// 	'94': function() {
// 		rom[rom[++register.PC] + register.X] = register.Y;
// 	},
// 	'95': function() {
// 		rom[rom[++register.PC] + register.X] = register.A;
// 	},
// 	'96': function() {
// 		rom[rom[++register.PC] + register.Y] = register.X;
// 	},
// 	'98': function() {
// 		register.A = register.Y;
// 	},
// 	'99': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y] = register.A;
// 	},
// 	'9A': function() {
// 		register.S = register.X;
// 	},
// 	'9D': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X] = register.A;
// 	},
// 	'A0': function() {
// 		register.Y = rom[++register.PC];
// 	},
// 	'A1': function() {
// 		register.A = rom[rom[rom[++register.PC] + register.X]];
// 	},
// 	'A2': function() {
// 		register.X = rom[++register.PC];
// 	},
// 	'A4': function() {
// 		register.Y = rom[rom[++register.PC]];
// 	},
// 	'A5': function() {
// 		register.A = rom[rom[++register.PC]];
// 	},
// 	'A6': function() {
// 		register.X = rom[rom[++register.PC]];
// 	},
// 	'A8': function() {
// 		register.Y = register.A;
// 	},
// 	'A9': function() {
// 		register.A = rom[++register.PC];
// 	},
// 	'AA': function() {
// 		register.X = register.A;
// 	},
// 	'AC': function() {
// 		register.Y = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))];
// 	},
// 	'AD': function() {
// 		register.A = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))];
// 	},
// 	'AE': function() {
// 		register.X = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))];
// 	},
// 	'B1': function() {
// 		register.A = rom[rom[rom[++register.PC]] + register.Y];
// 	},
// 	'B4': function() {
// 		register.Y = rom[rom[++register.PC] + register.X];
// 	},
// 	'B5': function() {
// 		register.A = rom[rom[++register.PC] + register.X];
// 	},
// 	'B6': function() {
// 		register.X = rom[rom[++register.PC] + register.Y];
// 	},
// 	'B9': function() {
// 		register.A = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y];
// 	},
// 	'BA': function() {
// 		register.X = register.S;
// 	},
// 	'BC': function() {
// 		register.Y = register.X = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X];
// 	},
// 	'BE': function() {
// 		register.X = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y];
// 	},
// 	'BD': function() {
// 		register.A = rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X];
// 	},
// 	'C6': function() {
// 		rom[rom[++register.PC]] -= 1;
// 	},
// 	'C8': function() {
// 		register.Y += 1;
// 	},
// 	'CA': function() {
// 		register.X -= 1;
// 	},
// 	'CE': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))] -= 1;
// 	},
// 	'D6': function() {
// 		rom[rom[++register.PC] + register.X] -= 1;
// 	},
// 	'D8': function() {
// 		flags.D8 = 0;
// 	},
// 	'DE': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X] -= 1;
// 	},
// 	'E1': function() {
// 		register.A += (flags.C - 1 - rom[rom[rom[++register.PC] + register.X]]);
// 	},
// 	'E5': function() {
// 		register.A += (flags.C - 1 - rom[rom[++register.PC]]);
// 	},
// 	'E6': function() {
// 		rom[rom[++register.PC]] += 1;
// 	},
// 	'E8': function() {
// 		register.X += 1;
// 	},
// 	'E9': function() {
// 		register.A += (flags.C - 1 - rom[++register.PC]);
// 	},
// 	'ED': function() {
// 		register.A += (flags.C - 1 - rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))]);
// 	},
// 	'EE': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff))] += 1;
// 	},
// 	'F1': function() {
// 		register.A += (flags.C - 1 - rom[rom[rom[++register.PC]] + register.Y]);
// 	},
// 	'F5': function() {
// 		register.A += (flags.C - 1 - rom[rom[++register.PC] + register.X]);
// 	},
// 	'F6': function() {
// 		rom[rom[++register.PC] + register.X] += 1;
// 	},
// 	'F9': function() {
// 		register.A += (flags.C - 1 - rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.Y]);
// 	},
// 	'FD': function() {
// 		register.A += (flags.C - 1 - rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X]);
// 	},
// 	'FE': function() {
// 		rom[(((rom[++register.PC] & 0xff) << 8) | (rom[++register.PC] & 0xff)) + register.X] += 1;
// 	}
// };