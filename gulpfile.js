var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
 
var paths = {
	js: ['src/*.js']
};

 gulp.task('default', ['js', 'watch']);

 gulp.task('js', function() {
  	return gulp.src(paths.js)
  		.pipe(concat('atari2600.js'))
    	.pipe(gulp.dest('output'))
      //.pipe(uglify())
      .pipe(rename({
        suffix: '.min'
      }))
      .pipe(gulp.dest('output'));
});

gulp.task('watch', function() {
	gulp.watch(paths.js, ['js']);
});